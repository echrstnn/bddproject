from math import log, sqrt
import sqlite3
import itertools


DB_FILE = 'hardware.db'
db, db_con = None, None


def db_execute(sql):
    print('>>> %s' % sql)
    db.execute(sql)


COMPAT2_COND = {
    ('cpu', 'fan'):         'c1.tdp <= c2.max_cpu_tdp',
    ('cpu', 'gpu'):         '1',
    ('cpu', 'hdd'):         '1',
    ('cpu', 'mb'):          '''c1.socket = (select socket from socket where socket.url=c2.url)
                                and (select chipset from chipset where chipset.url=c2.url) in (select chipset from chipset where chipset.url=c1.url)''',
    ('cpu', 'pccase'):      '1',
    ('cpu', 'psupply'):     '1',
    ('cpu', 'ram'):         '(select ram_freq from ram_freq where ram_freq.url=c2.url) in (select ram_freq from ram_freq where ram_freq.url=c1.url)',
    ('cpu', 'ssd'):         '1',
    ('fan', 'gpu'):         '1',
    ('fan', 'hdd'):         '1',
    ('fan', 'mb'):          '(select socket from socket where socket.url=c2.url) in (select socket from socket where socket.url=c1.url)',
    ('fan', 'pccase'):      'c1.height <= c2.fan_max_height',
    ('fan', 'psupply'):     '1',
    ('fan', 'ram'):         '1',
    ('fan', 'ssd'):         '1',
    ('gpu', 'hdd'):         '1',
    ('gpu', 'mb'):          '(select pcie_version from pcie_version where pcie_version.url=c1.url) in (select pcie_version from pcie_version where pcie_version.url=c2.url)',
    ('gpu', 'pccase'):      'c1.length <= c2.gpu_max_length',
    ('gpu', 'psupply'):     '1',
    ('gpu', 'ram'):         '1',
    ('gpu', 'ssd'):         '1',
    ('hdd', 'mb'):          '1',
    ('hdd', 'pccase'):      '1',
    ('hdd', 'psupply'):     '1',
    ('hdd', 'ram'):         '1',
    ('hdd', 'ssd'):         '1',
    ('mb', 'pccase'):       '(select mb_format from mb_format where mb_format.url=c1.url) in (select mb_format from mb_format where mb_format.url=c2.url)',
    ('mb', 'psupply'):      '1',
    ('mb', 'ram'):          '''(select ram_freq from ram_freq where ram_freq.url=c2.url) in (select ram_freq from ram_freq where ram_freq.url=c1.url)
                                and c2.modules <= c1.memory_slots
                                and c2.cap_per_module <= c1.memory_cap_per_slot
                                and c2.cap_per_module*c2.modules <= c1.memory_cap''',
    ('mb', 'ssd'):          'c1.m2 = (c2.format=\'M.2\')',
    ('pccase', 'psupply'):  'c2.length <= c1.psupply_max_length',
    ('pccase', 'ram'):      '1',
    ('pccase', 'ssd'):      '1',
    ('psupply', 'ram'):     '1',
    ('psupply', 'ssd'):     '1',
    ('ram', 'ssd'):         '1',
}


def gen_perf_rating(table, srccol):
    try:
        db_execute('alter table %s add column perf_rating int' % table)
        url_pm = db.execute('select url, %s from %s' % (srccol, table)).fetchall()
        max_pm = db.execute('select max(%s) from %s' %
                            (srccol, table)).fetchall()[0][0]
        pr_url = [(int(100 * pow(pm, 1/3) / pow(max_pm, 1/3))
                if pm is not None else None, url) for url, pm in url_pm]
        db.executemany('update %s set perf_rating = ? where url = ?' %
                    table, pr_url)
    except sqlite3.OperationalError:
        pass


def main():

    global db, db_con

    db_con = sqlite3.connect(DB_FILE)
    db = db_con.cursor()

    for table in ['cpu', 'gpu', 'hdd', 'ssd', 'ram', 'pccase', 'mb']:
        gen_perf_rating(table, 'passmark')
    gen_perf_rating('fan', 'max_cpu_tdp')
    gen_perf_rating('psupply', 'power')

    db_execute('create table if not exists compatible (url1 int, url2 int, primary key (url1, url2))')
    db_execute('delete from compatible')

    for t1, t2 in COMPAT2_COND:
        cond = COMPAT2_COND[t1, t2]
        db_execute('insert into compatible select c1.url, c2.url from %s as c1 join %s as c2 where (%s)' % (
            t1, t2, cond))

    db_execute('insert into compatible select url2, url1 from compatible')
    
    db_con.commit()
    db_execute('vacuum')
    db_con.close()


main()
