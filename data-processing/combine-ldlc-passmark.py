import time
import sqlite3
import re
import os


DBHW_FILE = 'hardware.db'
DBPM_FILE = '../webscraping/passmark.db'
DBLD_FILE = '../webscraping/ldlc.db'
dbhw, dbhw_con = None, None
dbpm, dbpm_con = None, None


def combine_entry(table, url, name, model):
    len_maxmatch = 5
    passmark_maxmatch = None
    pattern_maxmatch = None

    def trymatch(substr):
        nonlocal len_maxmatch, passmark_maxmatch, pattern_maxmatch
        pattern = '%%%s' % substr
        matches = dbpm.execute('select count(*), passmark from %s where name_std like "%s"' % (table, pattern))
        matches, passmark = matches.fetchall()[0]
        if matches != 1:
            pattern += '%'
            matches = dbpm.execute('select count(*), passmark from %s where name_std like "%s"' % (table, pattern))
            matches, passmark = matches.fetchall()[0]
        if matches == 1 and len_maxmatch < j-i:
            len_maxmatch = j-i
            pattern_maxmatch = pattern
            passmark_maxmatch = passmark
        return matches > 0
    
    i, j = 0, 0
    len_name = len(name)
    while j < len_name:
        if trymatch(name[i:j]):
            j += 1
        else:
            i += 1

    len_maxmatch = max(6, len_maxmatch / 2)
    i, j = 0, 0
    len_model = len(model)
    while j <= len_model:
        if trymatch(model[i:j]):
            j += 1
        else:
            i += 1
    if passmark_maxmatch is not None:
        dbhw.execute('update %s set passmark = ? where url = ?' % table, (passmark_maxmatch, url))
        print('Matched %s (%s) with pat \t %s' % (model, name, pattern_maxmatch))
    else:
        print('UNMATCHED %s (%s)'% (model, name))


def combine(table):
    for url, name, model in dbhw.execute('select url, name_std, model from %s' % table).fetchall():
        combine_entry(table, url, name, model)


def main():

    global dbhw, dbhw_con
    global dbpm, dbpm_con

    if os.path.exists(DBHW_FILE):
        os.remove(DBHW_FILE)

    dbhw_con = sqlite3.connect(DBHW_FILE)
    sqlite3.connect(DBLD_FILE).backup(dbhw_con)
    dbhw = dbhw_con.cursor()

    for table, in dbhw.execute('select name from sqlite_master where type="table"').fetchall():
        urls = dbhw.execute('select url from %s' % table).fetchall()
        rqparam = [(int(re.search('/en/product/PB(.*)\\.html', url).group(1)), url) for url, in urls]
        dbhw.executemany('update %s set url = ? where url = ?' % table, rqparam)
        cmd = dbhw.execute('select sql from sqlite_master where type=\'table\' and name=\'%s\'' % table).fetchall()[0][0]
        cmd = cmd.replace('url text', 'url int')
        dbhw.execute('alter table %s rename to %s2' % (table, table))
        dbhw.execute(cmd)
        dbhw.execute('insert into %s select * from %s2' % (table, table))
        dbhw.execute('drop table %s2' % table)

    dbpm_con = sqlite3.connect(DBPM_FILE)
    dbpm = dbpm_con.cursor()

    dbhw.execute('alter table gpu add column model text default ""')
    for i in ['hdd', 'ssd']:
        dbhw.execute(
            'update %s set capacity = 1000*capacity where capacity < 100' % i)
    for i in range(3, 6):
        dbhw.execute('create table ddr%d as select * from ram where url in (select url from ram_freq where ram_freq like "DDR%d%%")' % (i, i))

    for table in ['cpu', 'gpu', 'hdd', 'ssd', 'ddr3', 'ddr4', 'ddr5']:
        combine(table)
    
    dbhw.execute('delete from ram')
    for i in range(3, 6):
        dbhw.execute('insert into ram select * from ddr%d' % i)
        dbhw.execute('drop table ddr%d' % i)

    dbhw_con.commit()
    dbhw_con.close()


main()
