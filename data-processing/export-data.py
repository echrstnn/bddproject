from math import log, sqrt
import sqlite3
import os
import shutil
import time


DBIN_FILE = 'hardware.db'
DB_FILE = '../hardware.db'
dbin, dbin_con = None, None
db, db_con = None, None

IMG_DIR = '../webapp/static/webapp/prodimg/'

def main():

    global dbin, dbin_con
    global db, db_con

    if os.path.exists(DB_FILE):
        os.remove(DB_FILE)
    if os.path.exists(IMG_DIR):
        shutil.rmtree(IMG_DIR)
        os.mkdir(IMG_DIR)

    db_con = sqlite3.connect(DB_FILE)
    sqlite3.connect(DBIN_FILE).backup(db_con)
    db = db_con.cursor()

    for url, img in db.execute('select url, image from image').fetchall():
        imgpath = IMG_DIR + str(url) + '.jpg'
        print('Exporting %s' % imgpath)
        with open(imgpath, 'wb') as f:
            f.write(img)

    db.execute('drop table image')

    db_con.commit()
    db.execute('vacuum')
    db_con.close()


main()
