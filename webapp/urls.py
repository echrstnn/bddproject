from django.urls import path
from webapp import views

urlpatterns = [
    path('', views.accueil, name='accueil'),
    path('autoconfig', views.autoconfig, name='autoconfig'),
    path('explorer', views.explorer, name='explorer'),
]