import random
from math import ceil
from django.shortcuts import render
from django.http import HttpResponse
from django.db import connections
from django import forms


def dictfetchall(cur):
    nice_cols = {'url': 'Image', 'name': 'Nom du produit',
                 'passmark': 'Performance', 'price': 'Prix', 'cores': 'Coeurs', 'power': 'Puissance', 'max_cpu_tdp': 'Capacité de refroidissement'}
    cols = [col[0] for col in cur.description]
    head_raw = [col for col in cols if col in nice_cols]
    head = list(map(lambda c: nice_cols[c], head_raw))
    body = [dict(zip(cols, row)) for row in cur.fetchall()]
    return {'head': head, 'head_raw': head_raw, 'body': body}


def merge_rows(l):
    new_head = []
    new_head_raw = []
    new_body = []
    for row in l:
        h, hr, b = row['head'], row['head_raw'], row['body']
        new_body.extend(b)
        for i, ir in zip(h, hr):
            if ir not in new_head_raw:
                new_head.append(i)
                new_head_raw.append(ir)
    return {'head': new_head, 'head_raw': new_head_raw, 'body': new_body}
    

def stdstr(str):
    return ''.join(c.lower() for c in str if c.isalnum())


def accueil(request):
    return render(request, 'webapp/accueil.html')


class AutoConfigForm(forms.Form):
    ram = forms.IntegerField(label='RAM (Gb)', initial=8)
    ssd = forms.BooleanField(label='SSD', initial=False, required=False)
    gpu = forms.ChoiceField(label='GPU', choices=[('gpu', 'Dédié'), ('integrated', 'Intégré au CPU')])
    caseformat = forms.ChoiceField(label='Format boitier', choices=[(k, k) for k in ['Mid tower', 'Mini tower', 'Mini PC', 'Full tower', 'Desktop']])
    budget = forms.IntegerField(label='Budget', initial=900)


def genconfig(cur, form):
    spec_cond = {}
    spec_cond['psupply'] = lambda price: ('0.9*power >= 100 + '
        + '(select tdp from cpu where cpu.url=%d) + ' % best_for_price[price]['cpu']
        + ('(select consumption from gpu where gpu.url=%d)' % best_for_price[price]['gpu'] if form['gpu'] == 'gpu' else '50'))
    spec_cond['ram'] = lambda price: 'modules*cap_per_module >= %d' % form['ram']
    spec_cond['pccase'] = lambda price: 'case_format="%s"' % form['caseformat']

    types = ['cpu', 'mb']
    if form['gpu'] == 'gpu':
        types.append('gpu')
    else:
        spec_cond['cpu'] = lambda price: 'integrated_gpu'
    if form['ssd']:
        types.append('ssd')
    types.extend(['hdd', 'ram', 'fan', 'psupply', 'pccase'])

    score_w = {
        'cpu': 5,
        'fan': 0.5,
        'gpu': 4,
        'hdd': 1,
        'mb': 0,
        'pccase': 0,
        'psupply': 0.25,
        'ram': 1.5,
        'ssd': 2
    }

    _sum_score_w = sum(score_w.values())
    score_w = { k: v / _sum_score_w for k, v in score_w.items() }

    score_for_price = { 0: 0 }
    best_for_price = { 0: {} }

    max_price = form['budget']

    for itype in range(len(types)):
        type = types[itype]
        new_score_for_price = {}
        new_best_for_price = {}

        for price in score_for_price:
            sql = '''
                select url, price, perf_rating
                from %s
                ''' % type \
                + ' '.join('join compatible as c%d on c%d.url1 = %d and c%d.url2 = url' % (i, i, i, i) for i in best_for_price[price].values()) \
                + ' where price <= %d' % (max_price - price) \
                + (' and %s ' % spec_cond[type](price) if type in spec_cond else ' ')
            # print(sql)
            cur.execute(sql)
            for url, part_price, perf_rating in cur.fetchall():
                part_price = ceil(part_price)
                perf_rating = perf_rating if perf_rating is not None else 0
                new_price = price + part_price
                new_score = score_for_price[price] + score_w[type] * perf_rating
                if new_price not in new_score_for_price or new_score > new_score_for_price[new_price]:
                    new_score_for_price[new_price] = new_score
                    new_best_for_price[new_price] = best_for_price[price].copy()
                    new_best_for_price[new_price][type] = url

        score_for_price, best_for_price = new_score_for_price, new_best_for_price

    best = []
    best_score = -1
    for price in score_for_price:
        if score_for_price[price] > best_score:
            best, best_score = best_for_price[price], score_for_price[price]
    rows = []
    for i in best:
        cur.execute('select * from %s where url=%%s' % i, [best[i]])
        rows.append(dictfetchall(cur))
    return merge_rows(rows)


def autoconfig(request):
    context = {'form': AutoConfigForm()}
    if request.method == 'POST':
        form = AutoConfigForm(request.POST)
        if form.is_valid():
            context['form'] = form
            with connections['hw'].cursor() as cur:
                context['config'] = genconfig(cur, form.cleaned_data)
    return render(request, 'webapp/autoconfig.html', context)


class ExploreForm(forms.Form):
    category = forms.ChoiceField(label='Catégorie', choices=(
        ('cpu', 'CPU'),
        ('fan', 'Ventirad'),
        ('mb', 'Carte mère'),
        ('ram', 'RAM'),
        ('gpu', 'GPU'),
        ('hdd', 'Disque dur'),
        ('ssd', 'SSD'),
        ('psupply', 'Alimentation'),
        ('pccase', 'Boîtier')
    ))
    search = forms.CharField(label='Rechercher', max_length=30, required=False)
    sorting = forms.ChoiceField(label='Trier par', choices=[
        (raw + ascdesc, nice + arrow) for raw, nice in [
            ('perf_rating*perf_rating*perf_rating / price', 'Rapport qualité / prix'),
            ('price', 'Prix'),
            ('name', 'Nom')
        ] for ascdesc, arrow in [(' desc', ' ↘'), (' asc', ' ↗')]
    ])


def explorer(request):
    context = {'form': ExploreForm()}
    if request.method == 'POST':
        form = ExploreForm(request.POST)
        if form.is_valid():
            context['form'] = form
            with connections['hw'].cursor() as cur:
                search_pattern = '%%%s%%' % stdstr(form.cleaned_data['search'])
                cur.execute('''
                    select *
                    from %s
                    where
                        name_std like %%s
                    order by %s
                    ''' % (form.cleaned_data['category'], form.cleaned_data['sorting']), [search_pattern])
                context['component_list'] = dictfetchall(cur)
    return render(request, 'webapp/explorer.html', context)
