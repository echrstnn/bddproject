import time
import requests
from bs4 import BeautifulSoup
import sqlite3
import re
import os

CPU_LIST_URL = '/en/computing/components/processor/c4300/'
GPU_LIST_URL = '/en/computing/components/graphics-card/c4684'
CASE_LIST_URL = '/en/computing/components/pc-cases/c4678/'
PSUPPLY_LIST_URL = '/en/computing/components/pc-power-supply/c4289/'
HDD_LIST_URL = '/en/computing/components/internal-hard-drive/c4697/'
SSD_LIST_URL = '/en/computing/components/ssd/c4698/'
RAM_LIST_URL = '/en/computing/components/pc-ram/c4703/'
FAN_LIST_URL = '/en/computing/components/cpu-fan/c4712/'
MB_LIST_URL = '/en/computing/components/motherboard/c4293/'

CPU_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('model', 'processormodel', 'r'),
    ('frequency', ['frequencyinturbomode', 'cpufrequency'], 'f'),
    ('cores', 'numberofcores', 'i'),
    ('threads', 'numberofthreads', 'i'),
    ('tdp', 'tdp', 'i'),
    ('integrated_gpu', 'integratedgraphicscontroller', 'b'),
    ('socket', 'processorsupport', 'R'),
    ('chipset', 'motherboardchipsetcompatibility', 'R'),
    ('ram_freq', 'memoryfrequencyies', 'R'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

GPU_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('pcie_version', 'bus', 'I'),
    ('consumption', 'consumption', 'i'),
    ('length', 'length', 'i'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

PSUPPLY_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('power', 'power', 'i'),
    ('length', 'length', 'i'),
    ('certification', '80pluscertification', 'r'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

CASE_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('gpu_max_length', 'graphiccardmaxlength', 'i'),
    ('fan_max_height', 'maxheightcpufan', 'i'),
    ('psupply_max_length', 'powersupplymaxlength', 'i'),
    ('case_format', 'caseformat', 'r'),
    ('mb_format', 'motherboardformat', 'R'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

HDD_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('model', 'model', 's'),
    ('capacity', 'capacity', 'i'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

SSD_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('model', 'model', 's'),
    ('capacity', 'capacity', 'i'),
    ('format', 'harddriveformat', 'r'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

RAM_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('model', 'model', 's'),
    ('ram_freq', 'memoryfrequencyies', 'R'),
    ('format_pins', 'memoryformat', 'i'),
    ('modules', 'numberofmodules', 'i'),
    ('cap_per_module', 'capacitypermodule', 'i'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

FAN_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('type', 'coolingtype', 'r'),
    ('height', 'heightfanincluded', 'i'),
    ('cpu_max_tdp', 'maxcputdp', 'i'),
    ('socket', 'processorsupport', 'R'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

MB_ATTR = [
    ('name_std', 'productname', 's'),
    ('name', 'productname', 'r'),
    ('socket', 'processorsupport', 'R'),
    ('chipset', 'chipset', 'R'),
    ('mb_format', 'motherboardformat', 'R'),
    ('ram_freq', 'memoryfrequencyies', 'R'),
    ('memory_slots', 'memoryformat', 'i'),
    ('memory_cap_per_slot', 'maximumramcapacityperslot', 'i'),
    ('memory_cap', 'maximumramcapacity', 'i'),
    ('m2', 'harddrivesconnectors', 'm2'),
    ('pcie_version', 'graphicconnectors', 'FF'),
    ('passmark', 'price', 'n'),
    ('price', 'price', 'f')
]

ATTR_FUN = {
    'r': lambda x: x,
    's': lambda x: stdstr(x),
    'i': lambda x: int(re.search(r'\d+', x).group()),
    'n': lambda x: None,
    'f': lambda x: float(re.search(r'\d+(.\d+)?', x).group()),
    'b': lambda x: 1 if x.lower() == 'yes' else 0,
    'm2': lambda x: sum('M.2' in k for k in x) != 0,
    'ff': lambda x: int(float(re.search(r'\d+.\d+', x).group())) if re.search(r'\d+.\d+', x) is not None else None
}

ATTR_COLTYPE = {'r': 'text', 's': 'text',
                'i': 'int', 'n': 'int', 'f': 'real', 'b': 'int', 'ff': 'int', 'm2': 'int'}

DB_FILE = 'ldlc.db'
db, db_con = None, None

epoch = 0


def rq(url):
    global epoch, db, db_con
    epoch += 1
    time.sleep(0.3)
    if epoch % 5 == 0:
        print('[  ] >> AUTOSAVE')
        db_con.commit()
        db_con.close()
        db_con = sqlite3.connect(DB_FILE)
        db = db_con.cursor()
    print('[%d] >> GET %s' % (epoch, url))
    ans = requests.get(url)
    if ans.status_code != 200:
        raise ValueError
    return ans


def soup(url):
    return BeautifulSoup(rq('https://www.ldlc.com%s' % url).content, 'html.parser')


def stdstr(str):
    return ''.join(c.lower() for c in str if c.isalnum())


def cleanstr(str):
    return ' '.join(str.split())


def get_url_list(url):
    bs = soup(url)
    l = [(k.find('h3', 'title-3').a['href'], k.find('img')['src']) for k in bs.find_all('li', 'pdt-item')]
    next_url = bs.find('li', 'next')
    if next_url is not None:
        next_url = next_url.next['href']
        l.extend(get_url_list(next_url))
    return l


def get_component(url):
    bs = soup(url)
    tbl = bs.find('table', id='product-parameters')
    ans = {}
    ans['price'] = bs.find('div', 'wrap-aside').find('div',
                                                     'saleBlock invisible')['data-price']
    for tr in tbl.tbody.find_all('tr'):
        try:
            val = cleanstr(tr.find('td', 'checkbox').text)
            try:
                key = stdstr(tr.find('td', 'label').h3.text)
                ans[key] = val
            except AttributeError:
                if type(ans[key]) is not list:
                    ans[key] = [ans[key]]
                ans[key].append(val)
        except AttributeError:
            pass
    return ans


def parse_component(c, attr):
    ans = {}
    for newk, k, t in attr:
        src = None
        if type(k) is list:
            for k2 in reversed(k):
                if k2 in c:
                    src = c[k2]
            if src is None:
                raise KeyError(k)
        else:
            src = c[k]
        if t.isupper():
            if type(src) is not list:
                src = [src]
            ans[newk] = list(map(ATTR_FUN[t.lower()], src))
        else:
            ans[newk] = ATTR_FUN[t](src)
    return ans


def create_table(table, attr):
    attr_str = ', '.join('%s %s' % (
        k, ATTR_COLTYPE[t]) for k, _, t in attr if t.islower())
    db.execute('create table if not exists %s (url text primary key, %s)' %
               (table, attr_str))
    for k, _, t in attr:
        if t.isupper():
            db.execute('create table if not exists %s (url text, %s %s, primary key (url, %s))' % (
                k, k, ATTR_COLTYPE[t.lower()], k))


def add_component(url, table, attr):
    if db.execute('select count(*) from %s where url = ?' % table, (url,)).fetchall()[0][0] != 0:
        print('[  ] >> FOUND %s' % url)
        return
    c = get_component(url)
    try:
        c = parse_component(c, attr)
        c['url'] = url
        attr_str = ', '.join(':%s' % k for k, _, t in attr if t.islower())
        db.execute('insert into %s values (:url, %s )' % (table, attr_str), c)
        for k, _, t in attr:
            if t.isupper():
                db.executemany('insert or ignore into %s values (?, ?)' %
                               k, [(url, i) for i in c[k]])
    except KeyError as k:
        print('[  ] >> FAILED to find %s in ' % k, c)


def add_all_components(url, table, attr):
    create_table(table, attr)
    for i, _ in get_url_list(url):
        add_component(i, table, attr)


def add_all_images(urls):
    db.execute(
        'create table if not exists image (url text primary key, image blob)')
    for url in urls:
        for url, imgurl in get_url_list(url):
            if db.execute('select count(*) from image where url = ?', (url,)).fetchall()[0][0] != 0:
                print('[  ] >> FOUND %s' % url)
            else:
                img = rq(imgurl).content
                db.execute('insert into image values (?, ?)', (url, img))


def main():

    global db, db_con

    # if os.path.exists(DB_FILE):
    # os.remove(DB_FILE)

    db_con = sqlite3.connect(DB_FILE)
    db = db_con.cursor()

    # add_all_components(GPU_LIST_URL, 'gpu', GPU_ATTR)
    # add_all_components(CPU_LIST_URL, 'cpu', CPU_ATTR)
    # add_all_components(PSUPPLY_LIST_URL, 'psupply', PSUPPLY_ATTR)
    # add_all_components(CASE_LIST_URL, 'pccase', CASE_ATTR)
    # add_all_components(HDD_LIST_URL, 'hdd', HDD_ATTR)
    # add_all_components(SSD_LIST_URL, 'ssd', SSD_ATTR)
    # add_all_components(RAM_LIST_URL, 'ram', RAM_ATTR)
    # add_all_components(FAN_LIST_URL, 'fan', FAN_ATTR)
    add_all_components(MB_LIST_URL, 'mb', MB_ATTR)
    # add_all_images([GPU_LIST_URL, CPU_LIST_URL, PSUPPLY_LIST_URL, CASE_LIST_URL,
                #    HDD_LIST_URL, SSD_LIST_URL, RAM_LIST_URL, FAN_LIST_URL, MB_LIST_URL])

    db_con.commit()
    db_con.close()


main()
