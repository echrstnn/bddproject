from bs4 import BeautifulSoup
import sqlite3
import re
import os

PATH_MODEL = 'passmark-html/%s.html'

TABLES = ['cpu', 'gpu', 'hdd', 'ssd', 'ddr3', 'ddr4', 'ddr5']

DB_FILE = 'passmark.db'
db, db_con = None, None


def soup(path):
    with open(path) as f:
        return BeautifulSoup(f.read(), 'html.parser')


def stdstr(str):
    return ''.join(c.lower() for c in str if c.isalnum())


def cleanstr(str):
    return ' '.join(str.split())


def add_all_components(path, name_idx, passmark_fn, table_fn=None):
    print('Parsing %s' % path)
    if type(passmark_fn) is int:
        _idx = passmark_fn
        def passmark_fn(td): return int(stdstr(td[_idx].text))
    if table_fn is None:
        def table_fn(td): return path
    bs = soup(PATH_MODEL % path)
    tbl = bs.find('table', id='cputable').tbody
    for tr in tbl.find_all('tr', role='row'):
        td = tr.find_all('td')
        name = stdstr(td[name_idx].text.split('@')[0])
        passmark = passmark_fn(td)
        db.execute('insert into %s values (?, ?)' %
                   table_fn(td), (name, passmark))


def main():

    global db, db_con

    if os.path.exists(DB_FILE):
        os.remove(DB_FILE)

    db_con = sqlite3.connect(DB_FILE)
    db = db_con.cursor()

    for table in TABLES:
        db.execute('create table %s (name_std text, passmark int)' % table)

    def ram_passmark_fn(td): return int(
        100000/int(td[1].text) + int(stdstr(td[2].text)) + int(stdstr(td[3].text)))

    add_all_components('cpu', 1, 3)
    add_all_components('gpu', 1, 2)
    add_all_components('harddrive', 0, 2, lambda td: stdstr(td[3].text))
    add_all_components('ddr3', 0, ram_passmark_fn)
    add_all_components('ddr4', 0, ram_passmark_fn)
    add_all_components('ddr5', 0, ram_passmark_fn)

    db.execute('create temp table temp (name_std text, passmark int)')
    for table in TABLES:
        db.execute('delete from temp.temp')
        db.execute('insert into temp.temp select * from %s' % table)
        db.execute('drop table %s' % table)
        db.execute('create table %s (name_std text primary key, passmark int)' % table)
        db.execute(
            'insert into %s select name_std, cast(avg(passmark) as int) from temp.temp group by name_std' % table)
    db.execute('drop table temp.temp')

    db_con.commit()
    db_con.close()


main()
